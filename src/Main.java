import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Random;

public class Main {
    public static void main(String[] args) throws Exception{

        String nome = obterValorAleatorio("https://venson.net.br/resources/data/nomes.txt");
        String sobrenome = obterValorAleatorio("https://venson.net.br/resources/data/sobrenomes.txt");
        String posicao = obterValorAleatorio("https://venson.net.br/resources/data/posicoes.txt");
        String clube = obterValorAleatorio("https://venson.net.br/resources/data/clubes.txt");
        Number idade = gerarNumeroAleatorio();

        System.out.println(nome+" "+sobrenome+" é um futebolista brasileiro de "+idade+" anos que atua como "+posicao+". Atualmente defende o "+clube+".");
    }

    public static String obterValorAleatorio(String url) throws Exception {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(url)).build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        String texto = response.body();
        String[] lista = texto.split("\n");

        Random random = new Random();
        int indiceAleatorio = random.nextInt(lista.length);
        return lista[indiceAleatorio];
    }

    public static int gerarNumeroAleatorio() {
        Random random = new Random();
        int numeroAleatorio = random.nextInt(23) + 17;
        return numeroAleatorio;
    }
}